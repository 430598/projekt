﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Caliburn.Micro;
using Models;
using Newtonsoft.Json;
using ProjektUI.Models;
using APIXULib;
using ProjektUI.Views;

namespace ProjektUI.ViewModels
{
    public class MainViewModel: Screen
    {
       
        public Residence residence { get; set; }
        public BindableCollection<Camera> Cameras { get; set; }
        public BindableCollection<Recorder> Recorders { get; set; }
        public DelegateCommand addCameraDelegate { get; set; }
        public DelegateCommand delCameraDelegate { get; set; }
        public DelegateCommand addRecorderDelegate { get; set; }
        public DelegateCommand delRecorderDelegate { get; set; }
        public DelegateCommand residenceToFileDelegate { get; set; }
        public string title { get; set; }
        string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public string notes { get; set; }
        string key = "a8a08f71d39b41a8864162204192905";
        public string temp { get; set; }
        public string wind { get; set; }
        public string cond { get; set; }
        public string cloud { get; set; }
        public string precip { get; set; }

        public MainViewModel(string path)
        {
            title = path;
            
            residenceFromFile();
          
            connectToWeather();
           
            
            //deklaracje oddelegowanych metod
            addCameraDelegate = new DelegateCommand(addCamera);
            delCameraDelegate = new DelegateCommand(delCamera);
            addRecorderDelegate = new DelegateCommand(addRecorder);
            delRecorderDelegate = new DelegateCommand(delRecorder);
            residenceToFileDelegate = new DelegateCommand(residenceToFile);
            
           

        }
       
        public void addCamera(object obiekt)
        {
            Cameras.Add(new Camera(Cameras.Count + 1, "", ""));
            
            
        }
        public void delCamera(object obiekt)
        {
            int i = 1;
            Cameras.Remove((Camera)obiekt);
            foreach(Camera camera in Cameras)
            {
                camera.Id = i;
                i++;
            }
            Cameras.Refresh();
            
        }

        public void addRecorder(object obiekt)
        {
            Recorders.Add(new Recorder(Recorders.Count + 1, "", "", "", "", ""));
        }

        public void delRecorder(object obiekt)
        {
            int i = 1;
            Recorders.Remove((Recorder)obiekt);
            foreach(Recorder rec in Recorders)
            {
                rec.Id = i;
                i++;
            }
            Recorders.Refresh();
        }
        public void residenceToFile(object obiekt)
        {
            
            
                Task task = Task.Factory.StartNew(() =>
                {
                    try
                    {
                        using (StreamWriter file = File.CreateText(path + "//" + title + ".json"))
                        {
                            Residence residence = new Residence();
                            residence.Cameras = Cameras;
                            residence.Recorders = Recorders;
                            residence.Notes = notes;
                            JsonSerializer serializer = new JsonSerializer();
                            serializer.Serialize(file, residence);
                        }
                    }
                    catch (Exception e)
                    {
                        
                    }
                });
           
        }
        
        public void residenceFromFile()
        {
            Task<Residence> task = Task<Residence>.Factory.StartNew(() =>
            {
                residence = new Residence();
                try
                {
                    using (StreamReader file = File.OpenText(path + "//" + title + ".json"))
                    {

                        JsonSerializer serializer = new JsonSerializer();
                        residence = (Residence)serializer.Deserialize(file, typeof(Residence));

                    }
                }
                catch (Exception e)
                {

                }
                return residence;
            });
            residence = (Residence)task.Result;
            Cameras = residence.Cameras;
            Recorders = residence.Recorders;
            notes = residence.Notes;
        }
        public void connectToWeather()
        {
            try
            {
                IRepository repo = new Repository();
                WeatherModel weather = repo.GetWeatherDataByAutoIP(key);

                temp = weather.current.temp_c.ToString();
                precip = weather.current.precip_mm.ToString();
                wind = weather.current.wind_kph.ToString() + " " + weather.current.wind_dir.ToString();
                cond = weather.current.condition.text.ToString();
                cloud = weather.current.cloud.ToString();
            }catch(Exception e)
            {
                temp = "Brak danych";
            }

        }
    }
}


