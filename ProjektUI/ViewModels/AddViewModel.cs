﻿using ProjektUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektUI.ViewModels
{
    class AddViewModel
    {
        public DelegateCommand addItemDelegate { get; set; }
        string addedItem;

        public AddViewModel()
        {
            addItemDelegate = new DelegateCommand(addItem);
        }

        public void addItem(object item)
        {

            addedItem = (string)item;

          
        }


        public string getItem()
        {
            if (addedItem != null)
            {
                return addedItem;
            }
            else
            {
                return "";
            }
        }



    }





}
