﻿using Caliburn.Micro;
using Newtonsoft.Json;
using ProjektUI.Models;
using ProjektUI.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektUI.ViewModels
{
    
    public class StartViewModel : Screen
    {
        public DelegateCommand goToPathDelegate { get; set; }
        public DelegateCommand addItemDelegate { get; set; }
        public DelegateCommand deleteItemDelegate { get; set; }
        public BindableCollection<string> obiekty { get; set; }
        string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        string name;
        public StartViewModel()
        {


            
            
             FromFile();
                
            
          

            goToPathDelegate = new DelegateCommand(goToPath);
            addItemDelegate = new DelegateCommand(addItem);
            deleteItemDelegate = new DelegateCommand(deleteItem);
        }

        public void goToPath(object obiekt)
        {

            if (obiekt != null)
            {
                MainView mainWindow = new MainView((string)obiekt);
                mainWindow.Show();
            }

        }
        public void addItem(object obiekt)
        { 
            AddView addWindow = new AddView();
            addWindow.ShowDialog();
            try
            {
                name = ((AddViewModel)addWindow.DataContext).getItem();
                if (name !="")
                {
                    obiekty.Add(name);
                }
                toFile();
            }
            catch (Exception e)
            {

            }
        }

        public void deleteItem(object obiekt)
        {
            obiekty.Remove((string)obiekt);
            toFile();
        }
        public void toFile()
        {
           
            Task task = Task.Factory.StartNew(() =>
            {
                try
                {
                    using (StreamWriter file = File.CreateText(path + "/obiekty.json"))
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        serializer.Serialize(file, obiekty);

                    }
                }catch(Exception e)
                {

                }

            });
        }
        public void FromFile()
        {

            Task<BindableCollection<string>> task = Task<BindableCollection<string>>.Factory.StartNew(() =>
            {
                obiekty = new BindableCollection<string>();
                try
                {
                    using (StreamReader file = File.OpenText(path + "/obiekty.json"))
                    {
                        
                        JsonSerializer serializer = new JsonSerializer();
                        obiekty = (BindableCollection<string>)serializer.Deserialize(file, typeof(BindableCollection<string>));
                        
                    }

                }
                catch (Exception e)
                {

                }
                return obiekty;
            });
            obiekty = (BindableCollection<string>)task.Result;
        }
    }
}
