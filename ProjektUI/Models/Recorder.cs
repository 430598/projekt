﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektUI.Models
{
    public class Recorder : Camera
    {
        public Recorder(int id, string name, string ip,string ip2, string manufacturer, string model)
        {
            Id = id;
            Name = name;
            Ip = ip;
            Manufacturer = manufacturer;
            Model = model;
            Ip2 = ip2;
        }
        public Recorder() : base()
        {
            Ip2 = "";
        }

        private string ip2;

        public string Ip2
        {
            get { return ip2; }
            set { ip2 = value; }
        }


    }
}
