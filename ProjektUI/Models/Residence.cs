﻿using Caliburn.Micro;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektUI.Models
{
    public class Residence
    {
        public Residence()
        {
            Cameras = new BindableCollection<Camera>();
            Recorders = new BindableCollection<Recorder>();
            Notes = "";
        }
        private BindableCollection<Camera> cameras;

        public BindableCollection<Camera> Cameras
        {
            get { return cameras; }
            set { cameras = value; }
        }

        private BindableCollection<Recorder> recorders;

        public BindableCollection<Recorder> Recorders
        {
            get { return recorders; }
            set { recorders = value; }
        }

        private string notes;

        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }


    }
}
