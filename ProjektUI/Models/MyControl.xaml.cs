﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace ProjektUI.Models
{
    /// <summary>
    /// Logika interakcji dla klasy MyControl.xaml
    /// </summary>
    public partial class MyControl : System.Windows.Controls.UserControl
    {
        public MyControl()
        {
            InitializeComponent();
          
           
        }
        
        public static readonly DependencyProperty ImgSourceProperty =
         DependencyProperty.Register("ImgSource", typeof(ImageSource), typeof(MyControl), new
            UIPropertyMetadata(new BitmapImage(new Uri("/ProjektUI;component/Images/NoImageFound.png", UriKind.Relative)), new PropertyChangedCallback(OnImgSourceChanged)));

        public ImageSource ImgSource
        {
            get { return (ImageSource)GetValue(ImgSourceProperty); }
            set { SetValue(ImgSourceProperty, value); }
        }

        private static void OnImgSourceChanged(DependencyObject d,
           DependencyPropertyChangedEventArgs e)
        {
            MyControl mycontrol = (MyControl)d;
            mycontrol.OnImgSourceChanged(e);
        }

        private void OnImgSourceChanged(DependencyPropertyChangedEventArgs e)
        {

            Img.Source = (ImageSource)e.NewValue;
        }

        public void MouseClicked(object sender, RoutedEventArgs e)
        {
            string path;
            OpenFileDialog file = new OpenFileDialog();
            file.Filter = "Image Files|*.BMP;*.JPG;*.GIF;*.PNG";
            if (file.ShowDialog() == DialogResult.OK)
            {

                path = file.FileName;
                try { Img.Source = new BitmapImage(new Uri(path)); }
                catch(Exception ex)
                {

                }
            }
        }
        

    }
}
