﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Camera
    {
        public Camera()
        {
            Id = 0;
            Name = "";
            Ip = "";
            Manufacturer = "";
            Model = "";
        }
        public Camera(int id, string name, string ip)
        {
            Id = id;
            Name = name;
            Ip = ip;
            Manufacturer = "";
            Model = "";
        }
        public Camera(int id, string name, string ip, string manufacturer, string model)
        {
            Id = id;
            Name = name;
            Ip = ip;
            Manufacturer = manufacturer;
            Model = model;
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string ip;

        public string Ip
        {
            get { return ip; }
            set { ip = value; }
        }

        


        private string manufacturer;

        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }

        private string model;

        public string Model
        {
            get { return model; }
            set { model = value; }
        }



    }
}
